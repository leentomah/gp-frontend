export interface RecordTableColumns {
    id: "name"| "operation";
    label: string;
    minWidth?: number;
    align?: "right";
  }
  
  type RequestsData = {
    name: string;
    operation: string;
  };
   
  
  type RecordsData = {
    name:string;
    
  
  };

  type RecordApiResponse = {
    videos:RecordsData[];
  };
  
  export interface RecordsProps {
    usersRows: RecordsData[];
    usersColumns: RecordTableColumns[];
    requestsColumns: RecordTableColumns[];
    requestsRows: RequestsData[];
  }
  