export type CentersState = {
  id: number;
  name: string;
  centers: center[];
}[];

type center = {
  id: number;
  name: string;
  cameras: camera[];
};
type camera = {
  id: number;
  number: number;
};
