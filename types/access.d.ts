export interface AccessTableColumns {
  id: "username" | "role" | "operation" |"id";
  label: string;
  minWidth?: number;
  align?: "right";
}

type RequestsData = {
  username: string;
  operation: string;
};
type AccessApiResponse = {
  users:UsersData[];
};


type UsersData = {
  id:number;
  username: string;
  role: string;
  operation: string;

};

export interface AccessProps {
  usersRows: UsersData[];
  usersColumns: AccessTableColumns[];
  requestsColumns: AccessTableColumns[];
  requestsRows: RequestsData[];
}
