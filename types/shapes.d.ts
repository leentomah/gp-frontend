type style = {
  top: string;
  left: string;
  width: string;
  height: string;
};

type shapeProperties = {
  name: "triangle" | "line" | "circle" | "square";
  style: style;
};

export interface ShapesProps {
  properties: Array<shapeProperties>;
}
