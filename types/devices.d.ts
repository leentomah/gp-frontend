export interface CenterTableColumns {
  id: "port" | "ip" | "username" | "center" | "pass" | "operation";
  label: string;
  minWidth?: number;
  align?: "right";
}

type CenterData = {
  port: number;
  ip: string;
  center: string;
  username: string;
  pass: string;
  operation: string;
};

type DevicesApiResponse = {
  govs: Govs[];
  centers: Centers[];
};

type Govs = {
  id: number;
  name: string;
};

type Centers = {
  id: number;
  governorate_id: number;
  name: string;
  ip: string;
  username: string;
  password: string;
  port: number;
};
