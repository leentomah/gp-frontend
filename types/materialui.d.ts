export type MuiCssProps = {
  textColor?: string;
  backgroundColor?: string;
  width?: string;
  borderRadius?: string;
  margin?: string;
  height?: string;
  borderBottom?: string;
  borderBottomColor?: string;
  borderColor?: string;
  fontSize?: string;
  inputColor?: string;
  onHover?: {
    backgroundColor: string;
    borderColor: string;
  };
};
