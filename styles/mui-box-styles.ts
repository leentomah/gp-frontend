export const inputStyle = {
  "& label.Mui-focused": {
    color: "var(--main-color-top)",
  },
  "& .MuiInput-underline:after": {
    borderBottomColor: "var(--main-color-top)",
  },
  "& .MuiOutlinedInput-root": {
    "& fieldset": {
      color: "var(--main-color-top)",
    },
    "&:hover fieldset": {
      borderColor: "var(--main-color-top)",
    },
    "&.Mui-focused fieldset": {
      borderColor: "var(--main-color-top)",
    },
  },
};

export const buttonStyle = (styles: {
  textColor?: string;
  backgroundColor?: string;
  width?: string;
  margin?: string;
  height?: string;
  borderRadius?: string;
  hasBorderBottom?: boolean;
}) => ({
  ":hover": {
    backgroundColor: !styles.hasBorderBottom && "var(--main-color-bottom)",
  },
 
  color: styles.textColor || "var(--text-white)",
  backgroundColor: styles.backgroundColor || "var(--main-color-top)",
  width: styles.width || "clamp(100px,20%,200px)",
  margin: styles.margin || "15px auto",
  height: styles.height || "clamp(30px,8vh,40px)",
  borderRadius: styles.borderRadius || 1,
  borderBottom: styles.hasBorderBottom ? "1px solid #777" : "initial",
});
