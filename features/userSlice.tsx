import { createSlice } from "@reduxjs/toolkit";
import { UserState } from "types/reduxTypes";

const initialState: UserState = {
  value: {},
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setUser: (state, action) => {
      state.value = action.payload;
    },
  },
});

export const { setUser } = userSlice.actions;

export const selectUser = (state: any) => state.user.value;

export default userSlice.reducer;
