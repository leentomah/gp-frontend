const Config = {
  development: {
    apiUrl: "http://127.0.0.1:8000/api",
  },
  test: {
    apiUrl: "http://127.0.0.1:8000/api",
  },
  staging: {
    apiUrl: "http://127.0.0.1:8000/api",
  },
  production: {
    apiUrl: "http://127.0.0.1:8000/api",
  },
};

export default Config["development"];
