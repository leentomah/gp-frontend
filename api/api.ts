import Config from "./config";
import { FetchError } from "./error";
// import Config from 'config/Config';

export const corsFetchOptions: {
  mode?: RequestMode | undefined;
  credentials: RequestCredentials | undefined;
} = {
  // Enable cross-domain requests (CORS)
 // mode: "no-cors",
  // Send credentials cross-domain
  credentials: "include",
};

export const acceptJsonHeaders = {
  Accept: "application/json",
};

export const contentTypeJsonHeaders = {
  "Content-Type": "application/json",
};

export const authorizationHeaders = (token: string) => ({
  Authorization: `Token ${token}`,
});

// for future api
const sessionHeaders = (session: any) =>
  session ? authorizationHeaders(session) : {};

const endpointUrl = (Config: any, endpointUrl: string) => {
  return `${Config.apiUrl}${endpointUrl}`;
};

const checkStatus = async (response: Response) => {
  const data = await response.json();

  if (!response.ok) console.log("error");

  return data;
};

export function get<T>(
  url: string,
  options: { session?: unknown },
  image?: boolean,
  role?:boolean
): T | {} {
  
  let { session } = options;

  let headers;

    // const obj = JSON.parse('{"refresh":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTY1OTQ0MzI0NywiaWF0IjoxNjU5MzU2ODQ3LCJqdGkiOiJiNTEwNzk2NGQyMzI0Yzc2YTRmYzI2NTk2ODg4ZTg4ZSIsInVzZXJfaWQiOjF9.7YRqixuvfchOMEc1HyulqIgg3poO5cofIpCCQUD-Uvs","access":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjYxOTQ4ODQ3LCJpYXQiOjE2NTkzNTY4NDcsImp0aSI6IjE4MzNkYzI1ZThlYzQ0ODk4Y2Q0ZDlmZTY1NjRkMWUzIiwidXNlcl9pZCI6MX0.raRY7zaJjxZyZpXlLJyS72bDacW3bvR7kT174nbgS24"}');

    console.log(" role"+role)


  if (image) {
    headers = {
      "Content-Type": "multipart/x-mixed-replace",
      ...sessionHeaders(session),
      
    };
    console.log("in image")

  } else if(role) {
    console.log("in role")
    const TOKEN:string =localStorage.getItem("access")!;
    const {refresh,access}= JSON.parse(TOKEN);
      const JWTToken=access;
    headers = {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${JWTToken}`,
      "Cross-Origin-Opener-Policy":"same-origin",
      "Referrer-Policy":"same-origin",
    };
  }
  else
  {
    headers = {
      "Content-Type": "application/json",
      // "Authorization": `Bearer ${JWTToken}`,
      "Cross-Origin-Opener-Policy":"same-origin",
      "Referrer-Policy":"same-origin",
    };
    console.log("in else")

  }

  const finalUrl = endpointUrl(Config, url);

  if (image)
    return fetch(finalUrl, {
      method: "GET",
      headers,
    }).then((res) => res.blob());

  return fetch(finalUrl, {
    method: "GET",
    headers,
  }).then(checkStatus);
}



function requestWithBody<T>(
  method: "POST" | "PUT" | "PATCH",
  url: string,
  data: any,

  options: { session: unknown },
  login?:boolean
): T | {} {
  const { session } = options;
var headers;
if(login)
{   headers = {
  "Content-Type": "application/json",
};

}
else 
{
  const TOKEN:string =localStorage.getItem("access")!;
  // const obj = JSON.parse('{"refresh":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTY1OTQ0MzI0NywiaWF0IjoxNjU5MzU2ODQ3LCJqdGkiOiJiNTEwNzk2NGQyMzI0Yzc2YTRmYzI2NTk2ODg4ZTg4ZSIsInVzZXJfaWQiOjF9.7YRqixuvfchOMEc1HyulqIgg3poO5cofIpCCQUD-Uvs","access":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjYxOTQ4ODQ3LCJpYXQiOjE2NTkzNTY4NDcsImp0aSI6IjE4MzNkYzI1ZThlYzQ0ODk4Y2Q0ZDlmZTY1NjRkMWUzIiwidXNlcl9pZCI6MX0.raRY7zaJjxZyZpXlLJyS72bDacW3bvR7kT174nbgS24"}');
  const {refresh,access}= JSON.parse(TOKEN);
    const JWTToken=access;
    console.log(JWTToken,"JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ");

   headers = {
    "Content-Type": "application/json",
    "Authorization": `Bearer ${JWTToken}`,
  };
}
 
//  const x = localStorage.getItem("access")?.toString();
 
// console.log(TOKEN.access);
  
  const body = JSON.stringify(data);

  const finalUrl = endpointUrl(Config, url);

  return fetch(finalUrl, {
    method,
    body,
    headers,
  }).then(checkStatus);
}

export async function post<T>(url: string, data: any, options: any,login?:boolean): Promise<any> {
  return  requestWithBody("POST", url, data, options,login);
}
export function put<T>(url: string, data: any, options: any): T | {} {
  return requestWithBody("PUT", url, data, options);
}
export function patch<T>(url: string, data: any, options: any): T | {} {
  return requestWithBody("PATCH", url, data, options);
}

export function destroy<T>(
  url: string,
  options: { session: any }
): Promise<T | {}> {
  const { session } = options;

  const TOKEN =localStorage.getItem("access");
  const obj = JSON.parse('{"refresh":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTY1OTQ0MzI0NywiaWF0IjoxNjU5MzU2ODQ3LCJqdGkiOiJiNTEwNzk2NGQyMzI0Yzc2YTRmYzI2NTk2ODg4ZTg4ZSIsInVzZXJfaWQiOjF9.7YRqixuvfchOMEc1HyulqIgg3poO5cofIpCCQUD-Uvs","access":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjYxOTQ4ODQ3LCJpYXQiOjE2NTkzNTY4NDcsImp0aSI6IjE4MzNkYzI1ZThlYzQ0ODk4Y2Q0ZDlmZTY1NjRkMWUzIiwidXNlcl9pZCI6MX0.raRY7zaJjxZyZpXlLJyS72bDacW3bvR7kT174nbgS24"}');
  const JWTToken=obj.access;

  const headers = {
    ...acceptJsonHeaders,
    ...sessionHeaders(session),
    "Authorization": `Bearer ${JWTToken}`,
    "Cross-Origin-Opener-Policy":"same-origin",
    "Referrer-Policy":"same-origin",
    

  };
  const fetchOptions = {
    ...corsFetchOptions,
    method: "DELETE",
    headers,
  };

  const finalUrl = endpointUrl(Config, url);

  return fetch(finalUrl, fetchOptions).then(checkStatus);
}

export function deleteX<T>(url: string): T | {} {
  const TOKEN =localStorage.getItem("access");
  const obj = JSON.parse('{"refresh":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTY1OTQ0MzI0NywiaWF0IjoxNjU5MzU2ODQ3LCJqdGkiOiJiNTEwNzk2NGQyMzI0Yzc2YTRmYzI2NTk2ODg4ZTg4ZSIsInVzZXJfaWQiOjF9.7YRqixuvfchOMEc1HyulqIgg3poO5cofIpCCQUD-Uvs","access":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjYxOTQ4ODQ3LCJpYXQiOjE2NTkzNTY4NDcsImp0aSI6IjE4MzNkYzI1ZThlYzQ0ODk4Y2Q0ZDlmZTY1NjRkMWUzIiwidXNlcl9pZCI6MX0.raRY7zaJjxZyZpXlLJyS72bDacW3bvR7kT174nbgS24"}');
  const JWTToken=obj.access;
  const requestOptions = {
    method: 'DELETE',
    headers: { 
      "Authorization": `Bearer ${JWTToken}`,        
    }
  
};
const finalUrl = endpointUrl(Config, url);
console.log(finalUrl);


 return  fetch(finalUrl, requestOptions)
    .then(checkStatus);

}
