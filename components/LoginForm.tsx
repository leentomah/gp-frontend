import React from "react";
//
import { useRouter } from "next/router";
import {
  Alert,
  Box,
  Button,
  Snackbar,
  TextField,
  Typography,
} from "@mui/material";
import { buttonStyle, inputStyle } from "styles/mui-box-styles";
import { post,get } from "api/api";
import { useAppDispatch } from "features/app/hooks";
import { setUser } from "features/userSlice";
import { getFormData } from "utils/helpers";

const LoginForm = () => {
  const router = useRouter();
  const dispatch = useAppDispatch();
  const [toast, setToast] = React.useState(false);

  let handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    
    
    event.preventDefault();
    const data = getFormData(event);
    const resData = (await post<{ refresh: string; access: string }
      

      >(
      "/login/",
      data,
      {},true 
    )) as { refresh: string; access: string };
    
    await localStorage.setItem("access", JSON.stringify(resData));

    
    const role = await get<any>("/get_role",{},false,true) as {role:string};
    
    console.log(role,"roleeeeelele")

    console.log("role result",role.role)
    if (resData?.access) {
    console.log("🚀 ~ file: LoginForm.tsx ~ line 32 ~ handleSubmit ~ resData", resData.access)
      

      await localStorage.setItem("admin", role.role);

      dispatch(setUser(resData));
      router.push("/");
    }
    setToast(true);
  };

  const handleClose = (
    event?: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === "clickaway") {
      return;
    }
  };
  return (
    <Box
      sx={{
        overflow: "hidden",
        flex: 0.6,
        marginTop: 20,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <Typography component="h1" variant="h2" color={"var(--main-color-top)"}>
        Sign in To IMS
      </Typography>
      <Box
        component="form"
        onSubmit={handleSubmit}
        noValidate
        sx={{
          mt: 1,
          display: "flex",
          width: "70%",
          flexDirection: "column",
        }}
      >
        <TextField
          fullWidth
          sx={inputStyle}
          margin="normal"
          label="Username"
          name="username"
          autoComplete="username"
          autoFocus
        />
        <TextField
          margin="normal"
          sx={inputStyle}
          required
          fullWidth
          name="password"
          label="Password"
          type="password"
          id="password"
          autoComplete="current-password"
        />

        
        <Button
          type="submit"
          sx={buttonStyle({
            margin: "15px auto 0px",
            width: "clamp(200px,20%,400px)",
            borderRadius: "40px",
            height: "clamp(30px,8vh,120px)",
          })}
        >
          Sign In
        </Button>

        <Snackbar open={toast} autoHideDuration={6000} onClose={handleClose}>
          <Alert onClose={handleClose} severity="error" sx={{ width: "100%" }}>
            wrong username or password
          </Alert>
        </Snackbar>
      </Box>
    </Box>
  );
};

export default LoginForm;
