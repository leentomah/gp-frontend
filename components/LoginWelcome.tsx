import React from "react";
//
import Shapes from "components/Shapes";
//
import styles from "styles/loginWelcome.module.css";

const LoginWelcome: React.FC = () => {
  return (
    <div className={styles.loginWelcome}>
      <Shapes />
      <section className={styles.welcome_Section}>
        <h1>Welcome</h1>
        <p>
          Welcome to ICDL Monitoring System, Login if you have account, or
          demand new account from admin
        </p>
      </section>
    </div>
  );
};

export default LoginWelcome;
