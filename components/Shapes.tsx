import React from "react";
import styles from "styles/shapes.module.css";
const Shapes: React.FC = () => {
  return (
    <section className={styles.shapes}>
      <div className={styles.LineCircle}></div>
      <div className={styles.squareTringle}></div>
    </section>
  );
};

export default Shapes;
