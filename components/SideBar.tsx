import * as React from "react";
//
import Shapes from "components/Shapes";
import { MainListItems } from "components/ListItems";

//
import { styled } from "@mui/material/styles";
import MuiDrawer from "@mui/material/Drawer";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import { CentersState } from "types/home";
import { useAppSelector } from "features/app/hooks";
import { selectUser } from "features/userSlice";
import { useRouter } from "next/router";
import LogoutIcon from "@mui/icons-material/Logout";

const drawerWidth: string = "var(--sidebar-width)";

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  "& .MuiDrawer-paper": {
    height: "100vh",
    background: "var(--background-grad)",
    position: "relative",
    whiteSpace: "nowrap",
    overflowX: "hidden",

    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    boxSizing: "border-box",
    ...(!open && {
      overflowX: "hidden",
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      width: theme.spacing(7),
      [theme.breakpoints.up("sm")]: {
        width: theme.spacing(9),
      },
    }),
  },
}));

const SideBar: React.FC<{
  governorates?: CentersState;
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  cameras : any; 
  setCameras :any;
  
}> = ({ open, setOpen, governorates ,cameras,setCameras}) => {
  const [toast, setToast] = React.useState(false);

  const user = useAppSelector(selectUser);
  const router = useRouter();
  const toggleDrawer = () => {
    setOpen(!open);
  };

  return (
    <Box sx={{ display: "flex" }}>
      <Drawer
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "space-between",
        }}
        variant="permanent"
        open={open}
      >
        <Box sx={{ flex: 1 }}>
          <Toolbar
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            {open && (
              <>
                <h4 style={{ color: "var(--text-white)" }}>Logo</h4>
                <IconButton
                  sx={{ color: "var(--text-white)" }}
                  onClick={toggleDrawer}
                >
                  <ChevronLeftIcon />
                </IconButton>
              </>
            )}
          </Toolbar>
          <Divider />

          <List component="nav">
            <MainListItems governorates={governorates} cameras={cameras} setCameras={setCameras}  />
            <Divider sx={{ my: 1 }} />
          </List>
          <Shapes />
        </Box>

        <Box
          sx={{
            m: 2,
            borderBottom: "1px solid var(--text-white)",
          }}
        >
          {user && (
            <IconButton
              sx={{
                width: "100%",
                color: "var(--text-white)",
                fontSize: "20px",
              }}
              onClick={() => {
                router.push("/login");
              }}
            >
              {open ? "Log out" : <LogoutIcon />}
            </IconButton>
          )}
        </Box>
      </Drawer>
    </Box>
  );
};

export default SideBar;
