import React, { useEffect, useState } from "react";
import { useAppSelector } from "../features/app/hooks";
import { selectUser } from "../features/userSlice";
import { useRouter } from "next/router";
import { get } from "api/api";

const AuthGuard: React.FC = ({ children }) => {
  const user = useAppSelector(selectUser);
  const router = useRouter();
  const [authorized, setAuthorized] = useState<boolean>(false);

  useEffect(() => {
    const login = async () => {
      const data = await get("/login", {});
      console.log(data);
    };
  }, []);

  function AuthCheck(url: string) {
    // redirect to login page if accessing a private page and not logged in
    const publicPaths = ["/login"];
    const path = url.split("?")[0];
    if (!user.isLoggedIn && !publicPaths.includes(path)) {
      setAuthorized(false);
      router.push({
        pathname: "/login",
      });
    } else {
      setAuthorized(true);
    }
  }
  return <>{authorized && children}</>;
};

export default AuthGuard;
