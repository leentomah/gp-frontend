// react js imports
import { Fragment, useEffect, useState ,   } from "react";
import Link from "next/link";

//
import styles from "styles/listItems.module.css";
// material ui imports
import ListItemButton from "@mui/material/ListItemButton";
import FiberDvrIcon from "@mui/icons-material/FiberDvr";
import ListItemIcon from "@mui/material/ListItemIcon";
import LockIcon from "@mui/icons-material/Lock";
import ListItemText from "@mui/material/ListItemText";
import VideoCameraBackIcon from "@mui/icons-material/VideoCameraBack";
import RadarIcon from "@mui/icons-material/Radar";
import RadioButtonCheckedTwoToneIcon from '@mui/icons-material/RadioButtonCheckedTwoTone';
import {
  Checkbox,
  Collapse,
  FormControlLabel,
  FormGroup,
  List,
  ListItem,
  Typography,
} from "@mui/material";
import { ExpandLess, ExpandMore } from "@mui/icons-material";
import { CentersState } from "types/home";

export const MainListItems: React.FC<{
  governorates?: CentersState;
  cameras? : any; 
  setCameras ?:any;
}> = ({ governorates ,cameras,setCameras}) => {
  
  const [open, setOpen] = useState<any>({});
  const [camerasApi, setCamerasApi] = useState<any[]>([]);

  let isAdmin:boolean;

  if (typeof window !== 'undefined') {
    const role = localStorage.getItem('admin');
    console.log("role in list",role)
    isAdmin = role=="1"? true : false  ;
  }

  
  
  console.log("🚀 ~ file: ListItems.tsx ~ line 35 ~ camerasApi", camerasApi)
  
  
  const handleOpen = (index: any): void => {
  console.log("🚀 ~ file: ListItems.tsx ~ line 38 ~ handleOpen ~ index", index)
    let copy = { ...open };
    copy[index] = !open[index];
    setOpen(copy);
  };

  const handleAddCamera = (
    event: React.ChangeEvent<HTMLInputElement>,
    camera: string,
    cameraApi: string
    
  ) => {
    console.log("🚀 ~ file: ListItems.tsx ~ line 55 ~ event", event)
    let cams = [...cameras];
    console.log("🚀 ~ file: ListItems.tsx ~ line 50 ~ cams", cams)
    let camApi = [...camerasApi];
    console.log("🚀 ~ file: ListItems.tsx ~ line 52 ~ camApi", camApi)

    if (event.target.checked) {
      console.log("🚀 ~ file: ListItems.tsx ~ line 56 ~ camera", camera)
      let index = cams.indexOf(camera)
      index>-1?cams.splice(index,1): cams.push(camera);  
      camApi.push(cameraApi);
      setCameras(cams);
      setCamerasApi(cams);
     
      


    } else {
      cams = cams.filter((cam) => cam !== camera);
      setCameras(cams);      
      camApi = camApi.filter((api)=> api !== cameraApi);    
      setCamerasApi(camApi);
      

    }
    console.log("aaa",camera)
    console.log("bbbb",camApi )
  };

  return (
    <Fragment>
      <Link href="/" passHref>
        <ListItem
          className={`${styles.listItem__color} ${styles.listItem__nav}`}
          button
          component="a"
          onClick={() => handleOpen("liveView")}
        >
          <ListItemIcon className={`${styles.listItem__color}`}>
            <VideoCameraBackIcon />
          </ListItemIcon>
          <ListItemText
            primary={<Typography variant="h6">Live View</Typography>}
          />
          {open["liveView"] ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
      </Link>
      <Collapse
        orientation="horizontal"
        in={open["liveView"]}
        timeout="auto"
        unmountOnExit
      >
        <List
          component="div"
          disablePadding
          sx={{
            background: "var(--reverse-fade)",
            width: "var(--sidebar-width)",
          }}
        >
          {governorates &&
            governorates.map((governorate, i) => (
              <Fragment key={i + governorate.name}>
                <ListItemButton
                  className={`${styles.listItem__color}`}
                  onClick={() => handleOpen(governorate.name)}
                >
                  <ListItemText primary={governorate.name} />
                  {open[governorate.name] ? <ExpandLess /> : <ExpandMore />}
                </ListItemButton>
                <Collapse
                  orientation="horizontal"
                  in={open[governorate.name]}
                  timeout="auto"
                  unmountOnExit
                >
                  <List component="div" disablePadding>
                    {governorates[i].centers.map((center, j) => (
                      <Fragment key={i + j + center.id}>
                        <ListItemButton
                          className={`${styles.listItem__color}`}
                          onClick={() => handleOpen(center.id)}
                          sx={{
                            width: "var(--sidebar-width)",
                            pl: 10,
                            zIndex: 10,
                          }}
                        >
                          <ListItemText primary={center.name} />
                          {open[center.id] ? <ExpandLess /> : <ExpandMore />}
                        </ListItemButton>
                        <Collapse
                          orientation="horizontal"
                          in={open[center.id]}
                          timeout="auto"
                          unmountOnExit
                        >
                          {governorates[i]?.centers[j]?.cameras?.map(
                            (camera, k) => (
                              <FormGroup
                                sx={{
                                  width: "var( --sidebar-width)",
                                  display: "flex",
                                }}
                                key={i + j + k + camera.id}
                              >
                                {
                                  console.log("🚀 ~ file: ListItems.tsx ~ line 158 ~ camera", camera)
                                }
                                <FormControlLabel
                                  className={`${styles.listItem__color}`}
                                  sx={{
                                    pr: 2,
                                    zIndex: 10,
                                  }}
                                  labelPlacement="start"
                                  control={
                                    <Checkbox
                                    defaultChecked={camerasApi.find(el=>el=== camera.id.toString())}
                                       onChange={(
                                        e: React.ChangeEvent<HTMLInputElement>
                                      ) =>
                                        handleAddCamera(
                                          e,
                                          camera.id.toString(),
                                         ``

                                        )
                                      }
                                      color="info"
                                    />
                                  }
                                  label={camera.number}
                                />
                              </FormGroup>
                            )
                          )}
                        </Collapse>
                      </Fragment>
                    ))}
                  </List>
                </Collapse>
              </Fragment>
            ))}
        </List>
      </Collapse>
{ isAdmin &&
      <Link href="/devices" passHref>
        <ListItem
          className={`${styles.listItem__color} ${styles.listItem__nav}`}
          button
          component="a"
        >
          <ListItemIcon className={`${styles.listItem__color}`}>
            <FiberDvrIcon />
          </ListItemIcon>
          <ListItemText
            disableTypography
            primary={<Typography variant="h6">Devices</Typography>}
          />
        </ListItem>
      </Link>
}
{ isAdmin &&
      <Link href="/access" passHref>
        <ListItem
          className={`${styles.listItem__color} ${styles.listItem__nav}`}
          button
          component="a"
        >
          <ListItemIcon className={`${styles.listItem__color}`}>
            <LockIcon />
          </ListItemIcon>
          <ListItemText
            primary={<Typography variant="h6">Access</Typography>}
          />
        </ListItem>
      </Link>
}
{ isAdmin &&
      <Link href="/records" passHref>
        <ListItem
          className={`${styles.listItem__color} ${styles.listItem__nav}`}
          button
          component="a"
        >
          <ListItemIcon className={`${styles.listItem__color}`}>
            <RadioButtonCheckedTwoToneIcon />
          </ListItemIcon>
          <ListItemText
            primary={<Typography variant="h6">Records</Typography>}
          />
        </ListItem>
      </Link>
}
{ isAdmin &&
      <Link href="/detection" passHref>
        <ListItem
          className={`${styles.listItem__color} ${styles.listItem__nav}`}
          button
          component="a"
        >
          <ListItemIcon className={`${styles.listItem__color}`}>
            <RadarIcon />
          </ListItemIcon>
          <ListItemText
            primary={<Typography variant="h6">Detection</Typography>}
          />
        </ListItem>
      </Link>
}
    </Fragment>
  );
};
