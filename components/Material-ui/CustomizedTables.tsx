import React, { useEffect, useState } from "react";
//
import { devicesTableRows } from "jsonExamples/devices";
import { CenterData, CenterTableColumns } from "types/devices";
import { AccessTableColumns, RequestsData, UsersData } from "types/access";

import { inputStyle } from "styles/mui-box-styles";


//
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { styled } from "@mui/material/styles";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import DownloadRoundedIcon from '@mui/icons-material/DownloadRounded';
import {
  IconButton,
  Grid,
  Typography,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Box,

} from "@mui/material";
//
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

import { get, post, destroy, deleteX,put } from "api/api";
import { RecordsData, RecordTableColumns } from "types/records";

import {  createFileName } from "use-react-screenshot";
import { getFormData } from "utils/helpers";


const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    background: "var(--main-color-top)",
    color: theme.palette.common.white,
  },
}));



export const CustomizedTables: React.FC<{
  rowsNum: number;
  rows: CenterData[]  | UsersData[] | RecordsData[];
  columns: CenterTableColumns[] | AccessTableColumns[]|RecordTableColumns[];
  cusFrom: string;

}> = ({ rowsNum, rows, columns, cusFrom }) => {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState<number>(rowsNum);
  const [openDelete, setOpenDelete] = useState(false);
  const [openD, setOpen] = useState({ open: false });
  const [records , setRecords]= useState<any>([]);
  const [openEdit, setOpenEdit] = React.useState(false);
  
  const [openAccess, setAccess] = React.useState(false);
  const [idToDelete, setidToDelete] = useState();
  const [idToEdit, setidToEdit] = useState();
  const [rowInfo , setrowInfo]= useState<any>();



useEffect(()=>{
  setRecords(rows)
},[rows])


useEffect(()=>{
  let data   =  records.find(obj=>obj.id===idToEdit)
    console.log(data)
    setrowInfo(data);
},[idToEdit])



  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };
  //Delete 
  const handleDeleteClickOpen = (row: any) => {
    setOpenDelete(true);
    if(cusFrom=="records"){
      console.log(row)
      setidToDelete(row);
    }else
    setidToDelete(row.id);
  };
  const handleDeleteNO = () => {
    setOpenDelete(false)
  };

  const handleDeleteYes = async () => {
    console.log(`/DeleteCenter/${idToDelete}`)
    let newElemnts = records.filter((el:any)=>el.id!==idToDelete)
    setRecords(newElemnts)
    if (cusFrom == "device") {
      const del = await deleteX(`/DeleteCenter/${idToDelete}`);
    }
    else if(cusFrom == "access") {
      const del = await deleteX(`/DeleteUser/${idToDelete}`);

    }
    else if(cusFrom =="records"){
      

      const del = await deleteX(`/Delete/?name=${idToDelete}`);



    }

    setOpenDelete(false)
  };



  //Edit
  const handleEditClickOpen = async(id: any) => {
 
    setidToEdit(id) 
    setOpenEdit(true)
 
  };

  const handleEditSave =  async(event: React.FormEvent<HTMLFormElement>) => {
    
    event.preventDefault();
    const data = await  getFormData(event);

    console.log("data is ",data)
    await put<any>(
      "/updateCenter/?id="+idToEdit,
      data,
      {}
    )

    setOpenEdit(false);

  };

  //Access
  
  const handleAccessClose = () => {
    setAccess(false);

  };

const handleAccessSave =  async(event: React.FormEvent<HTMLFormElement>) => {

  
    
    event.preventDefault();
    const data = await  getFormData(event);

    console.log("data is ",data)
    await put<any>(
      "/updateUser/?id="+idToEdit,
      data,
      {}
    )

    setOpenEdit(false);

  };

  const handleDownload = (row) => {
    
    const x= `http://127.0.0.1:8000/api/Download/?name=${row}`
      const a = document.createElement("a");
      a.href = x;
      a.download = createFileName("mp4", `${row}`);
      a.click();
    
    
    //const x =  await get<any>(`/Download/?name=${row}`, {});


  };









  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    console.log(event.target.value);
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Paper>
      <Dialog
        open={openDelete}
        // onClose={()=>setOpenDelete({open:false})}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Are you sure you want to Delete this device?"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Press yes to confirm
          </DialogContentText>
        </DialogContent>
        <DialogActions>
        </DialogActions>
        <Button onClick={handleDeleteNO}>NO</Button>
        <Button onClick={handleDeleteYes} autoFocus color="error">
          YES
        </Button>
      </Dialog>


      {cusFrom=="device" &&


 
        <Dialog
          open={openEdit}
         // onClose={handleEditClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            {"Edit"}
          </DialogTitle>
          <Box
        component="form"
        onSubmit={handleEditSave}
        noValidate
        
      >
          <DialogContent>
       
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  name="ip"
                  fullWidth
                  size="small"
                  id="ip"
                  label="IP (ex:192.168.1.1)"
                  
                 defaultValue={rowInfo&&rowInfo.ip}            
                  
                />
              </Grid>

              <Grid item xs={6}>
                <TextField
                  fullWidth
                  size="small"
                  id="username"
                  label="username"
                  name="name"
                 
                  autoComplete="family-name"
                  defaultValue={rowInfo&&rowInfo.name}            

                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  fullWidth
                  size="small"
                  id="password"
                  label="password"
                  name="password"
                  autoComplete="password"
                  defaultValue={rowInfo&&rowInfo.password}            

                />
              </Grid>
              <Grid item xs={4}>
                <TextField
                  fullWidth
                  size="small"
                  id="governorates"
                  label="governorates"
                  name="governorates"
                  autoComplete="governorates"
                  defaultValue={rowInfo&&rowInfo.governorate_id}            

                />
              </Grid>

              <Grid item xs={4}>
                <TextField
                  size="small"
                  fullWidth
                  id="center"
                  label="Center name"
                  name="username"
                  defaultValue={rowInfo&&rowInfo.username}            

                />
              </Grid>
              <Grid item xs={4}>
                <TextField
                  size="small"
                  fullWidth
                  id="port"
                  label="Port"
                  name="port"
                  defaultValue={rowInfo&&rowInfo.port}            

                />
              </Grid>
            </Grid>
          </DialogContent>
          
          <DialogActions>
            <Button type="submit">Save</Button>
            <Button onClick={()=>{setOpenEdit(false)}}>Cancle</Button>

          </DialogActions>
          </Box>
        </Dialog>
        
      }

      {cusFrom=="access" &&

        <Dialog
          open={openEdit}
          onClose={handleAccessClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            {"Edit Access"}
          </DialogTitle>

          <Box
        component="form"
        onSubmit={handleAccessSave}
        noValidate
        
      >
          <DialogContent>

            <TextField
              sx={inputStyle}
              size="small"
              fullWidth
              label="username"
              name="username"
              margin="dense"
              defaultValue={rowInfo&&rowInfo.username} 
            />
            <TextField
              sx={inputStyle}
              size="small"
              fullWidth
              label="password"
              name="password"
              margin="dense"
              defaultValue={rowInfo&&rowInfo.password} 
            />
           
            <FormControl fullWidth>
              <InputLabel id="demo-simple-select-label">
                Role
              </InputLabel>
              <Select
                size="small"
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                label="Role"

                fullWidth
              >
                <MenuItem value={"employee"}>Employee</MenuItem>
                <MenuItem value={"admin"}>Admin</MenuItem>
              </Select>
            </FormControl>

          </DialogContent>
          <DialogActions>
            <Button type="submit">Save</Button>
            <Button onClick={()=>{setOpenEdit(false)}}>Cancle</Button>

          </DialogActions>
          </Box>
        </Dialog>



      }

      <TableContainer sx={{ overflowX: "initial" }}>
        <Table stickyHeader size="medium" aria-label="a dense table sticky">
          <TableHead
            sx={{
              background: "var(--main-color-top)",
            }}
          >
            <TableRow>
              {columns.map((column, index) => (
                <StyledTableCell
                  key={column.id + "_" + index}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </StyledTableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {records
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row: any, index) => {


                return (
                  
                  <TableRow
                    hover
                    role="checkbox"
                    tabIndex={-1}
                    key={row.name + "_" + index}
                  >
                    {columns.map((column, index) => {
                      let value = row[column?.id];
                      if(cusFrom=="records"){
                        value=row
                      }

                      return (
                        <TableCell
                          padding="checkbox"
                          key={column.id + "_" + index}
                          align={column.align}
                        >
                          {column.id !== "operation" ? (
                            value
                          ) : (
                            <>
                             {cusFrom != "records"&& 

                              <IconButton color="success" onClick={() => handleEditClickOpen(row.id)} >
                                <EditIcon />
                              </IconButton>
                    }

                             
                             
                              <IconButton color="error" onClick={() => handleDeleteClickOpen(row)}>
                                <DeleteIcon />
                              </IconButton>
                              
                              /
                              {cusFrom=="records"&&
                                <IconButton color="primary" onClick={()=>handleDownload(row)} >
                                <DownloadRoundedIcon />
                              </IconButton>
                              }



                            </>
                          )}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5]}
        component="div"
        count={devicesTableRows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Paper>
  );
};

export default CustomizedTables;
