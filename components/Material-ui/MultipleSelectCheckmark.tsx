import * as React from "react";
//
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import ListItemText from "@mui/material/ListItemText";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import Checkbox from "@mui/material/Checkbox";

const ITEM_HEIGHT = 40;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const MultipleSelectCheckmarks: React.FC<{
  selectName: string;
  selectState: string[];
  selectSetState: React.Dispatch<React.SetStateAction<string[]>>;
  keyValue: string[];
}> = ({ selectName, selectSetState, selectState, keyValue }) => {
  const handleChange = (event: SelectChangeEvent<typeof selectState>) => {
    const {
      target: { value },
    } = event;
    selectSetState(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };

  return (
    <div>
      <FormControl sx={{ m: 1, width: 150 }}>
        <InputLabel id="demo-multiple-checkbox-label">{selectName}</InputLabel>
        <Select
          sx={{ borderColor: "var(--main-color-top)" }}
          labelId="demo-multiple-checkbox-label"
          id="demo-multiple-checkbox"
          multiple
          value={selectState}
          onChange={handleChange}
          input={<OutlinedInput label={selectName} />}
          renderValue={(selected) => selected.join(", ")}
          MenuProps={MenuProps}
        >
          {keyValue.map((key) => (
            <MenuItem key={key} value={key}>
              <Checkbox checked={selectState.indexOf(key) > -1} />
              <ListItemText primary={key} />
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
};

export default MultipleSelectCheckmarks;
