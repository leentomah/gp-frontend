export const getFormData = (event: React.FormEvent<HTMLFormElement>) => {
  const form = event.target as HTMLFormElement;
  const elements = Array.from(form.elements) as HTMLInputElement[];
  const data = elements
    .filter((element) => element.hasAttribute("name"))
    .reduce(
      (object, element) => ({
        ...object,
        [`${element.getAttribute("name")}`]: element.value,
      }),
      {}
    );
    console.log(data);

  return data;
};

export function isEmpty(obj: any) {
  for (var prop in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, prop)) {
      return false;
    }
  }

  return JSON.stringify(obj) === JSON.stringify({});
}
