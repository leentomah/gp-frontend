import type { NextPage } from "next";
import Head from "next/head";
//
import LoginForm from "components/LoginForm";
import LoginWelcome from "components/LoginWelcome";
//
import { Box } from "@mui/material";

const Login: NextPage = () => {
  return (
    <div>
      <Head>
        <title>IMS-Login</title>
        <meta
          name="description"
          content="Login page to ICDL monitoring system"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Box
        sx={{
          position: "absolute",
          width: "100vw",
          height: "100vh",
          display: "flex",
        }}
      >
        <LoginWelcome />
        <LoginForm />
      </Box>
    </div>
  );
};

export default Login;
