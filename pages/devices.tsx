import React, { useState } from "react";
import Head from "next/head";
import type { GetServerSideProps, NextPage } from "next";
//
import CustomizedTables from "components/Material-ui/CustomizedTables";
import { buttonStyle, inputStyle } from "styles/mui-box-styles";
import MultipleSelectCheckmarks from "components/Material-ui/MultipleSelectCheckmark";
import SideBar from "components/SideBar";
import Header from "components/Header";
import AlertDialog from "components/DeletePopalert"

import { devicesColumns, devicesTableRows } from "jsonExamples/devices";
import {
  CenterTableColumns,
  CenterData,
  DevicesApiResponse,
} from "types/devices";
//
import {
  Alert,
  Button,
  Container,
  createTheme,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Paper,
  Snackbar,
  TextField,
  Toolbar,
  Typography,
} from "@mui/material";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import { Box } from "@mui/system";
import { ThemeProvider } from "@emotion/react";
import { get, post } from "api/api";
import { getFormData } from "utils/helpers";

const centersExample = ["alhadara", "alyarmook", "test", "layeek"];
const Devices: NextPage<{
  columns: CenterTableColumns[];
  rows: CenterData[];
  cameras : any; 
  setCameras :any;
  governorates: {
    id: number;
    name: string;
  }[];
  centers: {
    id: number;
    name: string;
  }[];
}> = ({ columns, rows, centers, governorates ,cameras,setCameras }) => {
  const [open, setOpen] = useState<boolean>(true);
  const [center, setCenter] = React.useState<string[]>([]);
  const [governorate, setGovernorate] = React.useState<string[]>([]);
  const theme = createTheme();
  const [toast, setToast] = React.useState({
    message: "",
    error: false,
    open: false,
  });

  const addCenterOrCamera = async (
    event: React.FormEvent<HTMLFormElement>,
    type: string
  ) => {
    event.preventDefault();
    const data: any = getFormData(event);
    let resData;
    console.log(data);
    if (type == "center")
      resData = (await post("/AddCenter/", data, {})) as { detail: string };
    else resData = (await post("/AddCamera/", data, {})) as { detail: string };

    if (resData?.detail) {
      setToast({
        message: resData?.detail,
        error: false,
        open: true,
      });
    } else {
      setToast({
        message: "please enter correct data",
        error: true,
        open: true,
      });
    }
  };

  const handleClose = (
    event?: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === "clickaway") return;
    setToast({ ...toast, open: false });
  };
  return (
    <div>
      <Head>
        <title>IMS-Devices</title>
        <meta name="description" content="Get Cameras" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <ThemeProvider theme={theme}>
        <Box sx={{ display: "flex" }}>
          <Header open={open} setOpen={setOpen}>
            <Box
              sx={{
                display: "flex",
                minWidth: 170,
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <Typography variant="h6" sx={{ color: "#000" }}>
                Filter Devices :
              </Typography>

              <MultipleSelectCheckmarks
                selectName="Centers"
                selectSetState={setCenter}
                selectState={center}
                keyValue={centersExample}
              />

              <MultipleSelectCheckmarks
                selectName="Governorate"
                selectSetState={setGovernorate}
                selectState={governorate}
                keyValue={centersExample}
              />

              <TextField type="number" label="Port" sx={inputStyle} />
            </Box>
          </Header>
          <SideBar open={open} setOpen={setOpen} cameras={cameras} setCameras={setCameras} />

          <Box
            component="main"
            sx={{
              flexGrow: 1,
              height: "var(--page-height)",
              padding: "var(--page-padding)",

              overflow: "auto",
            }}
          >
            <Toolbar />
            <Container maxWidth="xl" sx={{ mt: 1, mb: 1 }}>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <Paper
                    sx={{
                      display: "flex",
                      flexWrap: "wrap",
                      flexDirection: "column",
                      height: "calc(var(--half-page-height) + 5vh)",
                      overflow: "scroll",
                    }}
                  >
                    <CustomizedTables
                      rows={rows}
                      columns={columns}
                      rowsNum={5}
                      cusFrom = {"device"}
                    />
                  </Paper>
                </Grid>

                <Grid item xs={6}>
                  <Paper
                    sx={{
                      p: 2,
                      display: "flex",
                      flexWrap: "wrap",
                      flexDirection: "column",
                      height: "var(--half-page-height)",
                    }}
                  >
                    <Box
                      component="form"
                      onSubmit={(e: React.FormEvent<HTMLFormElement>) =>
                        addCenterOrCamera(e, "camera")
                      }
                      noValidate
                    >
                      <Typography variant="h6" mb={2}>
                        Add New Camera
                      </Typography>
                      <Grid container spacing={2}>
                        <Grid item xs={12}>
                          <FormControl fullWidth>
                            <InputLabel>Center</InputLabel>
                            <Select
                              size="small"
                              name="center"
                              defaultValue={1}
                              label="Center"
                            >
                              {centers.map((center, index) => (
                                <MenuItem
                                  key={index + "_" + center.name}
                                  value={center.id}
                                >
                                  {center.name}
                                </MenuItem>
                              ))}
                            </Select>
                          </FormControl>
                        </Grid>

                        <Grid item xs={12}>
                          <TextField
                            size="small"
                            fullWidth
                            type="number"
                            value={1}
                            id="dvr"
                            label="Dvr type"
                            name="dvr"
                          />
                        </Grid>
                        <Grid item xs={12}>
                          <TextField
                            fullWidth
                            size="small"
                            type="number"
                            label="number"
                            name="number"
                          />
                        </Grid>
                      </Grid>
                      <Button type="submit" sx={buttonStyle({})}>
                        Confrim
                      </Button>
                    </Box>
                  </Paper>
                </Grid>
                <Grid item xs={6}>
                  <Paper
                    sx={{
                      p: 2,
                      display: "flex",
                      flexWrap: "wrap",
                      flexDirection: "column",
                      height: "var(--half-page-height)",
                    }}
                  >
                    <Box
                      component="form"
                      onSubmit={(e: React.FormEvent<HTMLFormElement>) =>
                        addCenterOrCamera(e, "center")
                      }
                      noValidate
                    >
                      <Typography variant="h6" mb={2}>
                        Add New Center
                      </Typography>
                      <Grid container spacing={2}>
                        <Grid item xs={12}>
                          <TextField
                            name="ip"
                            fullWidth
                            size="small"
                            id="ip"
                            label="IP (ex:192.168.1.1)"
                            autoFocus
                          />
                        </Grid>

                        <Grid item xs={6}>
                          <TextField
                            fullWidth
                            size="small"
                            id="username"
                            label="username"
                            name="username"
                            autoComplete="family-name"
                          />
                        </Grid>
                        <Grid item xs={6}>
                          <TextField
                            fullWidth
                            size="small"
                            id="password"
                            label="password"
                            name="password"
                            autoComplete="password"
                          />
                        </Grid>
                        <Grid item xs={4}>
                          <FormControl fullWidth>
                            <InputLabel>Governorate</InputLabel>
                            <Select
                              size="small"
                              name="governorate"
                              defaultValue={1}
                              label="Governorate"
                            >
                              {governorates.map((gov, index) => (
                                <MenuItem
                                  key={index + "_" + gov.name}
                                  value={gov.id}
                                >
                                  {gov.name}
                                </MenuItem>
                              ))}
                            </Select>
                          </FormControl>
                        </Grid>
                        <Grid item xs={4}>
                          <TextField
                            size="small"
                            fullWidth
                            id="center"
                            label="Center name"
                            name="name"
                          />
                        </Grid>
                        <Grid item xs={4}>
                          <TextField
                            size="small"
                            fullWidth
                            id="port"
                            label="Port"
                            name="port"
                          />
                        </Grid>
                      </Grid>
                      <Button
                        type="submit"
                        sx={buttonStyle({ width: "clamp(120px,20%,200px)" })}
                      >
                        Add Center
                      </Button>
                    </Box>
                  </Paper>
                </Grid>
              </Grid>
            </Container>
          </Box>
          <Snackbar
            open={toast.open}
            autoHideDuration={6000}
            onClose={handleClose}
          >
            <Alert
              onClose={handleClose}
              severity={toast.error ? "error" : "success"}
              sx={{ width: "100%" }}
            >
              {toast.message}
            </Alert>
          </Snackbar>
        </Box>
      </ThemeProvider>
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  // console.log(localStorage.getItem("access"));

  const govsAndCenters = await get<DevicesApiResponse>("/Devices", {});

  const { govs, centers } = govsAndCenters as DevicesApiResponse;

  const centersKey = Object.keys(centers[0]);

  const centers_names = centers.map((center) => {
    return {
      name: center.name,
      id: center.id,
    };
  });

  const rows = centers;

  const columns = centersKey.map((key) => {
    return {
      id: key,
      label: key,
      minWidth: 130,
      align: "center",
    };
  });

  columns.push({
    id: "operation",
    label: "operation",
    minWidth: 140,
    align: "right",
  });

  return {
    props: {
      columns,
      rows,
      governorates: govs,
      centers: centers_names,
    }, // will be passed to the page component as props
  };
};

export default Devices;
