import type { GetServerSideProps, NextPage } from "next";
import Head from "next/head";
import React, { useEffect, useState ,createRef,useCallback, useRef } from "react";
//
import Header from "components/Header";
import SideBar from "components/SideBar";
//
import { buttonStyle, inputStyle } from "styles/mui-box-styles";
import VideocamOutlinedIcon from '@mui/icons-material/VideocamOutlined';
import VideocamOffOutlinedIcon from '@mui/icons-material/VideocamOffOutlined';
import ScreenshotMonitorOutlinedIcon from '@mui/icons-material/ScreenshotMonitorOutlined';

import { ThemeProvider } from "@emotion/react";
import {
  Container,
  createTheme,
  FormControl,
  Grid,
  MenuItem,
  Paper,
  Select,
  SelectChangeEvent,
  Toolbar,
  Typography,
  Button,
  IconButton,
  Checkbox,
} from "@mui/material";
import { Box } from "@mui/system";
import { get } from "api/api";
import { CentersState } from "types/home";
import { useScreenshot, createFileName } from "use-react-screenshot";
import { toPng ,toJpeg} from 'html-to-image';
import { saveAs } from 'file-saver';
import RecordRTC from 'recordrtc';
import html2canvas from 'html2canvas';


const Home: NextPage<{
  governorates: CentersState;
}> = ({ governorates }) => {
  const [open, setOpen] = useState<boolean>(true);
  const [gridSize, setGridSize] = useState<string>("1");
  const [stream, setStream] = useState<boolean>(false);
  const [cameras, setCameras] = useState<string[]>([]);
  const [state , setState ] = useState<any>(1)
  console.log("🚀 ~ file: index.tsx ~ line 46 ~ state", state.body)
  const theme = createTheme();
  const handleChangeSize = (event: SelectChangeEvent): void => {
    setGridSize(event.target.value as string);
  };


  
    
  
   
 
                                                        
   const ref:any = createRef()
   const [image, takeScreenshot] = useScreenshot({
    type: "image/jpeg",
    quality: 1.0
   }
  )

  const download = (image, { name = "img", extension = "jpg" } = {}) => {
    const a = document.createElement("a");
    a.href = image;
    a.download = createFileName(extension, name);
    a.click();
  };

  const downloadScreenshot = () => takeScreenshot(ref.current).then(download);
/////////  

  return (
    <div>
      <Head>
        <title>IMS-Live-view</title>
        <meta
          name="description"
          content="live view page for ICDL monitoring system"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      
      <ThemeProvider theme={theme}>
        <Box sx={{ display: "flex" }}>
          <Header open={open} setOpen={setOpen}>
            <Box
              sx={{
                display: "flex",
                minWidth: 170,
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <Typography variant="h6" sx={{ color: "#000" }}>
                Grid Size
              </Typography>
              <FormControl
                sx={{
                  background: "var(--background-grad)",
                }}
              >
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={gridSize}
                  label="Grid Size"
                  onChange={handleChangeSize}
                >
                  <MenuItem value={1}>1 * 1</MenuItem>
                  <MenuItem value={2}>2 * 2</MenuItem>
                  <MenuItem value={3}>3 * 3</MenuItem>
                  <MenuItem value={4}>4 * 4</MenuItem>
                </Select>
              </FormControl>
            </Box>
          
          </Header>

          <SideBar governorates={governorates} open={open} setOpen={setOpen} cameras={cameras} setCameras={setCameras}/>

          <Box
            component="main"
            sx={{
              flexGrow: 1,
              height: "100vh",
              overflow: "auto",
            }}
          >
            <Toolbar />
            <Container maxWidth="xl" sx={{ mt: 1, mb: 1 }}   >
              <Grid
                container
                spacing={2}
                style={{
                  transition: theme.transitions.create("all", {
                    easing: theme.transitions.easing.sharp,
                    duration: theme.transitions.duration.leavingScreen,
                  }),
                }}
              >
                {cameras && Array((parseInt(gridSize) * parseInt(gridSize)))
                .fill(0)
                  .map((_, index) => (
                    <Grid key={index} item xs={12 / parseInt(gridSize)} >   
                         

                      <div ref={ref}>                                
                      <Paper
                         sx={{
                          p: 2,
                          backgroundImage:`url(${cameras[index] != null?`http://127.0.0.1:8000/api/liveStream/${cameras[index]}`:'/temporarily-closed-sign.png'})`,
                          backgroundSize:"cover",
                          display: "flex",
                          flexDirection: "column",
                          height: `calc(88vh / ${parseInt(gridSize)} )`,
                          position: "relative",
                          overflow:"hidden"
                        }}
                      >  
                            
                        { 
 
                            
                      //  <img
                      //     style={{background:'red',width:'auto',height:'auto',objectFit:"fill"}}
                      //      src={cameras[index] != null?`http://127.0.0.1:8000/api/liveStream/${cameras[index]}`:'/temporarily-closed-sign.png'}
                         
                      //      id="element-to-record"

                            
                      //    />
                          }    
                      </Paper>
                      </div>
                       </Grid>
                      ) ) }

                
              </Grid>
            </Container>
          </Box>
        </Box>
      </ThemeProvider>
    </div>
  );
};

export default Home;



export const getServerSideProps: GetServerSideProps = async (context) => {




  const governorates = await get<CentersState | undefined>("/", {});
  return {
    props: {
      governorates,
    }, // will be passed to the page component as props
  };
};


