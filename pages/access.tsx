import type { GetServerSideProps, NextPage } from "next";
import Head from "next/head";
import React, { useState } from "react";
//
import Header from "components/Header";
import SideBar from "components/SideBar";
import MultipleSelectCheckmarks from "components/Material-ui/MultipleSelectCheckmark";
import { buttonStyle, inputStyle } from "styles/mui-box-styles";
import CustomizedTables from "components/Material-ui/CustomizedTables";

import {
  requestsColumns,
  requestsTableRows,
  usersColumns,
  usersTableRows,
} from "jsonExamples/access";

//
import { ThemeProvider } from "@emotion/react";
import {
  Button,
  Container,
  createTheme,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  TextField,
  Toolbar,
  Typography,
} from "@mui/material";
import { Box } from "@mui/system";
import { AccessProps,
  AccessApiResponse,
 } from "types/access";
////
import { get, post } from "api/api";
import { getFormData } from "utils/helpers";


const Access: NextPage<AccessProps> = ({
  requestsColumns,
  requestsRows,
  usersColumns,
  usersRows,

  
}) => {
  const [open, setOpen] = useState<boolean>(true);
  const [role, setRole] = useState<number>();
  const [center, setCenter] = React.useState<string[]>([]);
  const [governorate, setGovernorate] = React.useState<string[]>([]);
  const theme = createTheme();
  const [toast, setToast] = React.useState({
    message: "",
    error: false,
    open: false,
  });

  

  const addNewUser = async (
    event: React.FormEvent<HTMLFormElement>,
    
  ) => {
    event.preventDefault();
    const data: any = getFormData(event);
    let resData;
    
      resData = (await post("/CreateUser/", data, {})) as { detail: string };
      console.log(data);

    if (resData?.detail) {
      setToast({
        message: resData?.detail,
        error: false,
        open: true,
      });
    } else {
      setToast({
        message: "please enter correct data",
        error: true,
        open: true,
      });
    }
  };

  return (
    <div>
      <Head>
        <title>IMS-Access</title>
        <meta name="description" content="Access page only for admin users" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <ThemeProvider theme={theme}>
        <Box sx={{ display: "flex" }}>
          <Header open={open} setOpen={setOpen}>
            <Box
              sx={{
                display: "flex",

                minWidth: 170,
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <Typography variant="h5" sx={{ color: "#000" }}>
                FilterUser:  
              </Typography>

              <FormControl fullWidth>
                          <InputLabel id="demo-simple-select-label">
                            Role
                          </InputLabel>
                          <Select
                            size="medium"
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            label="Role"
                            value={role}
                            fullWidth
                          >
                            <MenuItem value={"employee"}>Employee</MenuItem>
                            <MenuItem value={"admin"}>Admin</MenuItem>
                          </Select>
                        </FormControl>
            </Box>
          </Header>
          <SideBar open={open} setOpen={setOpen} cameras setCameras />

          <Box
            component="main"
            sx={{
              flexGrow: 1,
              height: "var(--page-height)",
              padding: "var(--page-padding)",
              overflow: "auto",
            }}
          >
            <Toolbar />
            <Container maxWidth="xl" sx={{ mt: 1, mb: 1 }}>
              <Grid container spacing={2}>
                <Grid item xs={5}>  

                    <Grid item xs={11}>
                      <Paper
                        sx={{
                          p: 1,
                          display: "flex",
                          flexWrap: "wrap",
                          flexDirection: "column",
                          height: "var(--half-page-height)",
                          
                        }}
                      >
                        <Box
                      component="form"
                      onSubmit={(e: React.FormEvent<HTMLFormElement>) =>
                        addNewUser(e)
                      }
                      noValidate
                    >
                        <Typography variant="h6">Add New User</Typography>
                        <TextField
                          sx={inputStyle}
                          size="small"
                          fullWidth
                          label="username"
                          margin="dense"
                          name="username"

                      
                        />
                        <TextField
                          sx={inputStyle}
                          size="small"
                          fullWidth
                          label="email"
                          margin="dense"
                          name="email"

                        />
                        <TextField
                          sx={inputStyle}
                          size="small"
                          fullWidth
                          label="password"
                          margin="dense"
                          name="password"
                        />
                        <TextField
                          sx={inputStyle}
                          size="small"
                          fullWidth
                          label="Confirm password"
                          margin="dense"
                        />
                        
                        <FormControl fullWidth>
                          <InputLabel id="demo-simple-select-label">
                            Role
                          </InputLabel>
                          <Select
                            size="small"
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            label="Role"
                            value={0}
                            fullWidth
                            name="is_superuser"
                          >
                            <MenuItem value={0} >Employee</MenuItem>
                            <MenuItem value={1} >Admin</MenuItem>
                          </Select>
                        </FormControl>

                        <Button
                          type="submit"
                          sx={buttonStyle({
                            width: "clamp(200px,20%,250px)",
                            margin: "12px auto 0px",                            
                          }) }

                        >
                          Submit
                        </Button>
                        </Box>
                      </Paper>
                      
                  </Grid>
                </Grid>

                <Grid item xs={7}>
                  <Paper
                    sx={{
                      display: "flex",
                      flexWrap: "wrap",
                      flexDirection: "column",
                      height: "var( --max-content-height)",
                      overflow: "scroll",
                    }}
                  >
                    <CustomizedTables
                      rows={usersRows}
                      columns={usersColumns}
                      rowsNum={usersRows.length}
                      cusFrom ={"access"}
                    />
                  </Paper>
                </Grid>
              </Grid>
            </Container>
            
          </Box>
        </Box>
      </ThemeProvider>
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const usersList = await get<AccessApiResponse>("/Access", {});
  const { users } = usersList as AccessApiResponse;
  console.log(users);

  const userKyes = Object.keys(users[0]);

  const columns = userKyes.map((key) => {
    return {
      id: key,
      label: key,
      minWidth: 130,
      align: "center",
    };
  });


  const rows=users;

  const userColumns = usersColumns;
  const usersRows = usersTableRows;
  const requestColumns = requestsColumns;
  const requestsRows = requestsTableRows;
  return {
    props: {
      usersRows:rows,
      usersColumns: usersColumns,
      requestsColumns: requestColumns,
      requestsRows,
    }, // will be passed to the page component as props
  };
};

export default Access;
