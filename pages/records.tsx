import type { GetServerSideProps, NextPage } from "next";
import Head from "next/head";
import React, { useState } from "react";
//
import Header from "components/Header";
import SideBar from "components/SideBar";
import MultipleSelectCheckmarks from "components/Material-ui/MultipleSelectCheckmark";
import { buttonStyle, inputStyle } from "styles/mui-box-styles";
import CustomizedTables from "components/Material-ui/CustomizedTables";

import {
  requestsColumns,
  requestsTableRows,
  usersColumns,
  usersTableRows,
} from "jsonExamples/records";

//
import { ThemeProvider } from "@emotion/react";
import {
  Button,
  Container,
  createTheme,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  TextField,
  Toolbar,
  Typography,
} from "@mui/material";
import { Box } from "@mui/system";
import { RecordTableColumns,
  RequestsData,
  RecordsData,
  RecordsProps,
  RecordApiResponse,
  
 } from "types/records";
////
import { get, post } from "api/api";
import { getFormData } from "utils/helpers";


const Records: NextPage<RecordsProps> = ({
  requestsColumns,
  requestsRows,
  usersColumns,
  usersRows,

  
}) => {
  const [open, setOpen] = useState<boolean>(true);
  const [role, setRole] = useState<number>();
  const [center, setCenter] = React.useState<string[]>([]);
  const [governorate, setGovernorate] = React.useState<string[]>([]);
  const theme = createTheme();
  const [toast, setToast] = React.useState({
    message: "",
    error: false,
    open: false,
  });

  console.log("userRows",usersRows);



  return (
    <div>
      <Head>
        <title>IMS-Records</title>
        <meta name="description" content="Records page only for admin users" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <ThemeProvider theme={theme}>
        <Box sx={{ display: "flex" }}>
          
          <SideBar open={open} setOpen={setOpen} cameras setCameras />

          <Box
            component="main"
            sx={{
              flexGrow: 1,
              height: "var(--page-height)",
              padding: "var(--page-padding)",
              overflow: "auto",
            }}
          >
            <Toolbar />
            <Container maxWidth="xl" sx={{ mt: 1, mb: 1 }}>
              <Grid container >
                
                <Grid item xs={12}>
                  <Paper
                    sx={{
                      display: "flex",
                      flexWrap: "wrap",
                      flexDirection: "column",
                      height: "var( --max-content-height)",
                      overflow: "scroll",
                    }}
                  >
                    <CustomizedTables
                      rows={usersRows}
                      columns={[{ id: "id", label: "Name", minWidth: 100 },{id: "operation", label: "operation", minWidth: 140,align: "right",}]}
                      rowsNum={20}
                      cusFrom ={"records"}
                    />
                  </Paper>
                </Grid>
              </Grid>
            </Container>
            
          </Box>
        </Box>
      </ThemeProvider>
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const videoList = await get<RecordApiResponse>("/Videos", {});
  const { videos } = videoList as RecordApiResponse ;
  console.log("aaaaa",videos);

 /* const userKyes = Object.keys(videos[0]);

  const columns = userKyes.map((key) => {
    return {
      id: key,
      label: key,
      minWidth: 130,
      align: "center",
    };
  });*/
  const rows=videos;

  const userColumns = usersColumns;
  const usersRows = usersTableRows;
  const requestColumns = requestsColumns;
  const requestsRows = requestsTableRows;
  return {
    props: {
      usersRows:rows,
      usersColumns,
      requestsColumns,
      requestsRows,
    }, // will be passed to the page component as props
  };
};

export default Records;
