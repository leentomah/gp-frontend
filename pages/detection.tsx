import type { GetServerSideProps, NextPage } from "next";
import Head from "next/head";
import React, { useState } from "react";
//
import Header from "components/Header";
import SideBar from "components/SideBar";
//
import VideocamOutlinedIcon from "@mui/icons-material/VideocamOutlined";
import ScreenshotMonitorOutlinedIcon from "@mui/icons-material/ScreenshotMonitorOutlined";

import { ThemeProvider } from "@emotion/react";
import {
  Container,
  createTheme,
  FormControl,
  Grid,
  MenuItem,
  Paper,
  Select,
  SelectChangeEvent,
  Toolbar,
  Typography,
  IconButton,
} from "@mui/material";
import { Box } from "@mui/system";
import { get } from "api/api";
import { CentersState } from "types/home";

const Detection: NextPage<{
  governorates: CentersState;
}> = ({ governorates }) => {
  const [open, setOpen] = useState<boolean>(true);
  const [gridSize, setGridSize] = useState<string>("1");
  const [cameras, setCameras] = useState<string[]>([]);
  const [state, setState] = useState<any>(1);
  const [vids, setvids] = useState<string[]>(["", "", "", ""]);
  const theme = createTheme();
  const handleChangeSize = (event: SelectChangeEvent): void => {
    setGridSize(event.target.value as string);
  };

  const getVideoWithDetection = (index) => {
    var videos = [...vids];
    if (videos[index] === "")
      videos[index] = `http://127.0.0.1:8000/api/getvideo/${index + 1}`;
    else videos[index] = "";
    setvids(videos);
  };

  return (
    <div>
      <Head>
        <title>IMS-Detection</title>
        <meta
          name="description"
          content="Detection page for ICDL monitoring system"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <ThemeProvider theme={theme}>
        <Box sx={{ display: "flex" }}>
          <Header open={open} setOpen={setOpen}>
            <Box
              sx={{
                display: "flex",
                minWidth: 170,
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <Typography variant="h6" sx={{ color: "#000" }}>
                Grid Size
              </Typography>
              <FormControl
                sx={{
                  background: "var(--background-grad)",
                }}
              >
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={gridSize}
                  label="Grid Size"
                  onChange={handleChangeSize}
                >
                  <MenuItem value={1}>1 * 1</MenuItem>
                  <MenuItem value={2}>2 * 2</MenuItem>
                </Select>
              </FormControl>
            </Box>
          </Header>

          <SideBar
            governorates={governorates}
            open={open}
            setOpen={setOpen}
            cameras={cameras}
            setCameras={setCameras}
          />

          <Box
            component="main"
            sx={{
              flexGrow: 1,
              height: "100vh",
              overflow: "auto",
            }}
          >
            <Toolbar />
            <Container maxWidth="xl" sx={{ mt: 1, mb: 1 }}>
              <Grid
                container
                spacing={2}
                style={{
                  transition: theme.transitions.create("all", {
                    easing: theme.transitions.easing.sharp,
                    duration: theme.transitions.duration.leavingScreen,
                  }),
                }}
              >
                {cameras &&
                  Array(parseInt(gridSize) * parseInt(gridSize))
                    .fill(0)
                    .map((_, index) => (
                      <Grid key={index} item xs={12 / parseInt(gridSize)}>
                        <IconButton
                          onClick={() => getVideoWithDetection(index)}
                          size="large"
                          sx={{
                            color: "rgba(56, 105, 151, 0.84)",
                            ":focus": {
                              color: "red",
                              backgroundColor: "white",
                            },
                          }}
                        >
                          <VideocamOutlinedIcon />
                        </IconButton>

                        <Paper
                          sx={{
                            p: 2,
                            background: "#F9F9F9",
                            display: "flex",
                            flexDirection: "column",
                            height: `calc(80vh / ${parseInt(gridSize)} )`,
                            position: "relative",
                          }}
                        >
                          {
                            <img
                              src={
                                vids[index] != ""
                                  ? vids[index]
                                  : "/temporarily-closed-sign.png"
                              }
                              width="100%"
                              height="100%"
                              id="element-to-record"
                            />
                          }
                        </Paper>
                      </Grid>
                    ))}
              </Grid>
            </Container>
          </Box>
        </Box>
      </ThemeProvider>
    </div>
  );
};

export default Detection;

export const getServerSideProps: GetServerSideProps = async (context) => {
  const governorates = await get<CentersState | undefined>("/", {});
  return {
    props: {
      governorates,
    }, // will be passed to the page component as props
  };
};
