import { RecordTableColumns, RecordsData, RequestsData } from "types/records";

export const usersTableRows: RecordsData[] = Array(30)
  .fill("")
  .map((entry, _index) => {
    return {
      
      name: "vid" + _index,
      
    };
  });

export const requestsTableRows: RequestsData[] = Array(30)
  .fill("")
  .map((entry, _index) => {
    return {
      name: "vid" + _index,
      operation: "",
    };
  });
export const usersColumns: readonly RecordTableColumns[] = [
  { id: "name", label: "Name", minWidth: 100 },
  
  {
    id: "operation",
    label: "operation",
    minWidth: 140,
    align: "right",
  },
];

export const requestsColumns: readonly RecordTableColumns[] = [
  { id: "name", label: "Name", minWidth: 140 },
  {
    id: "operation",
    label: "operation",
    minWidth: 140,
    align: "right",
  },
];
