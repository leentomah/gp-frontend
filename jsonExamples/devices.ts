import { CenterData, CenterTableColumns } from "types/devices";

export const devicesTableRows: CenterData[] = Array(30)
  .fill("")
  .map((entry, _index) => {
    return {
      name: "dvr" + _index,
      port: 554 + _index,
      ip: "1324171354" + _index,
      center: "alhadara" + _index,
      username: "ali" + _index,
      pass: "123",
      operation: "",
    };
  });

export const devicesColumns: readonly CenterTableColumns[] = [
  { id: "port", label: "port", minWidth: 100 },
  {
    id: "ip",
    label: "ip",
    minWidth: 140,
    align: "right",
  },
  {
    id: "center",
    label: "center",
    minWidth: 140,
    align: "right",
  },

  {
    id: "username",
    label: "username",
    minWidth: 140,
    align: "right",
  },
  {
    id: "pass",
    label: "pass",
    minWidth: 140,
    align: "right",
  },
  {
    id: "operation",
    label: "operation",
    minWidth: 140,
    align: "right",
  },
];
