import { AccessTableColumns, UsersData, RequestsData } from "types/access";

export const usersTableRows: UsersData[] = Array(20)
  .fill("")
  .map((entry, _index) => {
    return {
      id:_index,
      username: "dvr" + _index,
      role: "admin" + _index,
      operation: "",
    };
  });

export const requestsTableRows: RequestsData[] = Array(20)
  .fill("")
  .map((entry, _index) => {
    return {
      username: "dvr" + _index,
      operation: "",
    };
  });
export const usersColumns: readonly AccessTableColumns[] = [
  { id: "id", label: "ID", minWidth: 100 },
  { id: "username", label: "Username", minWidth: 140 },
 
  {
    id: "operation",
    label: "operation",
    minWidth: 140,
    align: "right",
  },
];

export const requestsColumns: readonly AccessTableColumns[] = [
  { id: "username", label: "Username", minWidth: 140 },
  {
    id: "operation",
    label: "operation",
    minWidth: 140,
    align: "right",
  },
];
